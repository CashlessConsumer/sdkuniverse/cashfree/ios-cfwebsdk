# Changelog
All Notable changes to this project will be Documented in this file.

## v2.1.6 (18-08-2022)

### Changed 
1. Bug Fixes.

## v2.1.5 (05-05-2022)

### Changed
1. Axis Bank redirection bug fix

## v2.1.4 (19-04-2022)

### Added
1. Ordering of UPI Apps in web checkout

### Changed
1. User agent Bug fix

## v2.1.2 (30-12-2021)

### Changed

1. Back Navigation Bug fix for paypal payment

## v2.1.0 (31-08-2021)

### Changed

1. To ensure proper payment responses, we have disabled cancel button functionality when the payment is being verified.

## v2.0.9 (02-06-2021)

### Added
1. Testing with Payment Simulator (UPI Intent) in TEST environment now follows the same process as that of PROD environment payment. The users will get back responses along with signature and referenceID.
2. Introduced BHIM application in UPI Intent (Follow documentation to support BHIM).

## v2.0.7 (29-04-2021)

### Added
1. Changed UI of the Unified UPI Intent.
2. Direct UPI App Integration in web-checkout flow.
3. Added a simulator for testing UPI Integration in TEST environment.
4. Bug Fixes.
