
[![No Maintenance Intended](https://img.shields.io/badge/deprecated-No_maintenance_intended-red)](https://img.shields.io/badge/deprecated-No_maintenance_intended-red)

This is no longer supported, please consider using [this](https://docs.cashfree.com/docs/ios-native) instead. New merchants will not able able to collect payments using this SDK.

# Cashfree SDK Integration Steps

### Please refer our official android documentation [here](https://docs.cashfree.com/docs/ios-native).

### Discord
Join our [discord server](https://discord.gg/znT6X45qDS) to instantly get connected
